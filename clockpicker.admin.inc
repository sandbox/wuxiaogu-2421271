<?php
/**
 * @file
 * Administration and configuration forms.
 */

/**
 * Default settings configuration form.
 * url: admin/config/stanford-datetimepicker
 * @param  array $form see form api
 * @param  array $form_state see form api
 * @return array the form array.
 */
function clockpicker_config_form($form, &$form_state) {
  $form = array();

  $form['timepicker_date_pop_time_part'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Timepicker to integrate with Date Popup module'),
    '#default_value' => variable_get('timepicker_date_pop_time_part', TRUE),
  );

  $form['date_pop_up_time_input_format'] = array(
    '#title' => t('Time Input Format'),
    '#type' => 'fieldset',
    '#states' => array(
      'invisible' => array(
       ':input[name="timepicker_date_pop_time_part"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['date_pop_up_time_input_format']['date_pop_up_time_donetext'] = array(
    '#title' => t('Donetext'),
    '#type' => 'textfield',
    '#size' => 15,
    '#default_value' => variable_get('date_pop_up_time_donetext','Done'),
  );

  $form['date_pop_up_time_input_format']['date_pop_up_time_placement'] = array(
    '#title' => t('my placement'),
    '#type' => 'select',
    '#default_value' => variable_get('date_pop_up_time_placement','top'),
    '#options' => drupal_map_assoc(array('top', 'left', 'bottom', 'right')),
    // '#description' => t('Corner of the clockpicker widget dialog to position.'),
  );

  $form['date_pop_up_time_input_format']['date_pop_up_time_align'] = array(
    '#title' => t('my align'),
    '#type' => 'select',
    '#default_value' => variable_get('date_pop_up_time_align','left'),
    '#options' => drupal_map_assoc(array('left', 'right')),
    // '#description' => t('Corner of the clockpicker widget dialog to position.'),
  );

  $form['date_pop_up_time_input_format']['date_pop_up_time_autoclose'] = array(
    '#title' => t('Autoclose'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('date_pop_up_time_autoclose',FALSE),
  );

  $form['date_pop_up_time_input_format']['date_pop_up_time_default'] = array(
    '#type' => 'textfield',
    '#title' => t('init time'),
    '#size' => 15,
    '#default_value' => variable_get('date_pop_up_time_default', '13:14'),
  );

  $form['date_pop_up_time_input_format']['date_pop_up_time_vibrate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Vibrate'),
    '#default_value' => variable_get('date_pop_up_time_vibrate', FALSE),
  );

  return system_settings_form($form);
}

