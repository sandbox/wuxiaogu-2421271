<?php

/**
 * @file
 * Template file for timefield
 *
 * variables available:
 *
 * $item['time'] - the formatted output of this field
 */

?>
<div class="clockpicker-default">
<?php print $item['time'] ?>
</div>
