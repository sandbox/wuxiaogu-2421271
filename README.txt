DESCRIPTION
--------------

Clockpicker is a Field API field that integrate with jquery library
jquery-clockpicker.min.js.

The goal of this field is to provide a time picker widget like a real clock.

INSTALLATION
------------

1) Start by copying the module files into your 'modules' directory.  For more
information: http://drupal.org/documentation/install/modules-themes/modules-7

2) The Clockpicker module utilizes a jQuery plugin to assist with the input of
time values.  The use of this plugin is facilitated by the Libraries module. If
not already installed, download, enable, and configure the latest versions of
the Libraries module. See:
http://drupal.org/project/libraries

3) Acquire the clockpicker plugin.
You can download the plugin using url http://weareoutman.github.io/clockpicker.
Make sure the plugin path is:

site-root/sites/all/libraries/clockpicker/dist/jquery-clockpicker.min.js
site-root/sites/all/libraries/clockpicker/dist/jquery-clockpicker.min.css

4) Acquire jqury version 1.7 or higher.So Jquery Update module is recommended.
See:http://drupal.org/project/jquery_update

USAGE
-------
Clockpicker module provides a field type 'clockpicker' with a default JQuery widget.

First,you should add a new field, choose Clockpicker Field field type and choose
Clockpicker Field widget.

