/**
 * @file
 * Javascript for Timefield.
 */

(function ($) {
  Drupal.behaviors.clockpicker = {
    attach: function(context, settings) {
      // Iterate over timefield settings, which keyed by input class.
      for (var element in settings.clockpicker) {
        // Attach timepicker behavior to each matching element.
        $("input.edit-field-clockpicker", context).each(function(index) {
          $(this).clockpicker(settings.clockpicker[element]);
        });
      }

      // 整合date pup-up
      $("input.popup-time-clockpicker", context).clockpicker(
        settings.date_pop_up_time_js_settings
      );
    }
  };
})(jQuery);
